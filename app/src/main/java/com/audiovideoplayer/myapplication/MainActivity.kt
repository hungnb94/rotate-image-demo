package com.audiovideoplayer.myapplication

import android.Manifest
import android.content.pm.PackageManager
import android.media.ExifInterface
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.core.app.ActivityCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.Rotate
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity() {
    private val TAG = "ExifInterfaceTest"
    private var rotation = 0
    private var initRotate = 0

    private val directory: File = Environment.getExternalStorageDirectory()
    private val file = File(directory, "big.jpg")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        }
        initRotate = getOrientation()
        showImage()

        btnRotate.setOnClickListener {
            rotation += 90
            if (rotation >= 360) rotation = 0

            showImage()
        }

        btnRestore.setOnClickListener {
            val currRotation = getOrientation()
            if (initRotate == currRotation) {
                rotation = 0
            } else {
                rotation = initRotate
            }
            showImage()
        }
    }

    override fun onPause() {
        super.onPause()
        Log.e(TAG, "Pause and save")
        val exif = ExifInterface(file.absolutePath)

        val rotate = (rotation + initRotate) % 360

        val newRotation = when (rotate) {
            0 -> ExifInterface.ORIENTATION_NORMAL
            90 -> ExifInterface.ORIENTATION_ROTATE_90
            180 -> ExifInterface.ORIENTATION_ROTATE_180
            270 -> ExifInterface.ORIENTATION_ROTATE_270
            else -> ExifInterface.ORIENTATION_NORMAL
        }
        exif.setAttribute(ExifInterface.TAG_ORIENTATION, newRotation.toString())
        exif.saveAttributes()
    }

    private fun getOrientation(): Int {
        val exif = ExifInterface(file.absolutePath)
        val orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
        )
        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_270 -> 270
            ExifInterface.ORIENTATION_ROTATE_180 -> 180
            ExifInterface.ORIENTATION_ROTATE_90 -> 90
            else -> 0
        }

    }

    private fun showImage() {
        val requestOptions = RequestOptions()
                .transform(Rotate(rotation))

        Glide.with(this)
                .load(file.path)
                .apply(requestOptions)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(imageView)
    }
}
